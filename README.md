# Simple to-do list application
This is a simple to-do list app built with `Angular`

## License
Developed under [the MIT license](https://opensource.org/licenses/MIT).