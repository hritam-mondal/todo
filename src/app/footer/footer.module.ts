import { NgModule } from '@angular/core';

import { FooterComponent } from 'src/app/footer/component/footer/footer.component';

@NgModule({
    declarations: [
        FooterComponent
    ],
    exports: [
        FooterComponent
    ]
})
export class FooterModule { }