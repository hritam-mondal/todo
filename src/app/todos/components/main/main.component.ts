import { Component } from '@angular/core';
import { combineLatestWith, map, Observable } from 'rxjs';

import { FilterEnum } from 'src/app/todos/enums/filter.enum';
import { TodoInterface } from 'src/app/todos/interfaces/todo.interface';
import { TodosService } from 'src/app/todos/services/todos.service';


@Component({
    selector: 'app-todos-main',
    templateUrl: './main.component.html'
})
export class MainComponent {
    visibleTodos$!: Observable<TodoInterface[]>
    noTodoClass$!: Observable<boolean>
    isAllTodosSelected$!: Observable<boolean>
    editingId!: string | null;

    constructor(private todosService: TodosService) {
        this.noTodoClass$ = this.todosService.todos$.pipe(map(todos => todos.length === 0))
        this.isAllTodosSelected$ = this.todosService.todos$.pipe(map(todos => todos.every(todo => todo.isCompleted)))
        this.visibleTodos$ = this.todosService.todos$.pipe(
            combineLatestWith(this.todosService.filter$),
            map(([todos, filter]: [TodoInterface[], FilterEnum]) => {
                if (filter === FilterEnum.active) {
                    return todos.filter((todo) => !todo.isCompleted);
                } else if (filter === FilterEnum.completed) {
                    return todos.filter((todo) => todo.isCompleted);
                }
                return todos;
            })
        )
    }

    toggleAllTodos(event: Event): void {
        const target = event.target as HTMLInputElement;
        this.todosService.toggleAll(target.checked);
    }

    setEditingId(editingId: string | null): void {
        this.editingId = editingId
    }
}