import { Component } from '@angular/core';
import { map, Observable } from 'rxjs';

import { TodosService } from 'src/app/todos/services/todos.service';
import { FilterEnum } from 'src/app/todos/enums/filter.enum';

@Component({
    selector: 'app-todos-footer',
    templateUrl: './footer.component.html'
})
export class FooterComponent {
    activeCount$!: Observable<number>
    noTodosClass$!: Observable<boolean>
    itemsLeftText$!: Observable<string>
    filter$: Observable<FilterEnum>
    showClearCompleted$!: Observable<boolean>
    filterEnum = FilterEnum

    constructor(private todosService: TodosService) {
        this.showClearCompleted$ = this.todosService.todos$.pipe(map(todos => todos.every(todo => todo.isCompleted)))
        this.activeCount$ = this.todosService.todos$.pipe(
            map(todos => todos.filter(todos => !todos.isCompleted).length)
        )
        this.itemsLeftText$ = this.activeCount$.pipe(
            map(activeCount => `item${activeCount > 1 ? 's' : ''} left`)
        )
        this.noTodosClass$ = this.todosService.todos$.pipe(
            map(todos => todos.length === 0)
        )
        this.filter$ = this.todosService.filter$;
    }

    changeFilter(event: Event, filterName: FilterEnum): void {
        event.preventDefault();
        this.todosService.changeFilter(filterName);
    }

    clearCompleted():void {
        this.todosService.clearCompleted()
    }
}