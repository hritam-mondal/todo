import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'

import { TodoInterface } from 'src/app/todos/interfaces/todo.interface'
import { FilterEnum } from 'src/app/todos/enums/filter.enum'

@Injectable()
export class TodosService {
    todos$ = new BehaviorSubject<TodoInterface[]>([])
    filter$ = new BehaviorSubject<FilterEnum>(FilterEnum.all)

    addTodo(text: string): void {
        if (text != '') {
            const newTodo: TodoInterface = {
                text,
                isCompleted: false,
                id: Math.random().toString(16)
            }
            const updatedTodos = [...this.todos$.getValue(), newTodo]
            this.todos$.next(updatedTodos)
        }
    }

    toggleAll(isCompleted: boolean): void {
        const updatedTodos = this.todos$.getValue().map(todo => {
            return {
                ...todo,
                isCompleted
            }
        })
        this.todos$.next(updatedTodos)
    }

    changeTodo(id: string, text: string): void {
        if (text == '') {
            this.removeTodo(id)
            return;
        }
        const updatedTodos = this.todos$.getValue().map(todo => {
            if (todo.id === id) {
                return {
                    ...todo,
                    text
                }
            }
            return todo
        })
        this.todos$.next(updatedTodos)
    }

    removeTodo(id: string): void {
        const updatedTodos = this.todos$.getValue().filter(todo => todo.id !== id)
        this.todos$.next(updatedTodos)
    }

    clearCompleted(): void {
        const updatedTodos = this.todos$.getValue().filter(todo => todo.isCompleted !== true)
        this.todos$.next(updatedTodos)
    }

    toggleTodo(id: string): void {
        const updatedTodos = this.todos$.getValue().map(todo => {
            if (todo.id === id) {
                return {
                    ...todo,
                    isCompleted: !todo.isCompleted
                }
            }
            return todo
        })
        this.todos$.next(updatedTodos)
    }

    changeFilter(filterName: FilterEnum): void {
        this.filter$.next(filterName);
    }
}